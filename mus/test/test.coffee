chai = require 'chai'
chai.should()

exports = require "../compiler.js"

describe 'mus',->
  melody_mus =
    tag: 'seq'
    left:
      tag: 'par'
      left:
        tag: 'note', pitch: 'c4', dur: 250
      right:
        tag: 'note', pitch: 'c3', dur: 500
    right:
      tag: 'par'
      left:
        tag: 'note', pitch: 'c3', dur: 500
      right:
        tag: 'note', pitch: 'c3', dur: 250
  notes = exports.compile(melody_mus)

  it 'Should support syntax parse tree',->
    notes.length.should.equal 4
  it 'Should parse picth',->
    notes[0].pitch.should.equal 60

