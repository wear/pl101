var endTime = function (time, expr) {
  var total_dur = 0;
  switch(expr.tag){
  case 'note':
    total_dur = expr.dur;
    break;
  case 'rest':
    total_dur = expr.duration;
    break;
  case 'repeat':
    total_dur = endTime(time,expr.section);
    break;
  case 'seq':
    total_dur = endTime(0,expr.left) + endTime(0,expr.right);
    break;
  case 'par':
    var leftTime = endTime(0,expr.left);
    var rightTime = endTime(0,expr.right);
    if(leftTime > rightTime){
      total_dur = leftTime;
    } else {
      total_dur = rightTime;
    }
    break;
  }
  return time + total_dur;
};

var convertPitch = function(pitch){
  var num;
  var letterPitch = pitch.split('')[0];
  var octave = parseInt(pitch.split('')[1]);
  midiChart = {
    c:0,
    d:2,
    b:11
  }
  num = 12 + 12 * octave + midiChart[letterPitch];
  return num;
}

var compileT = function(time,musexpr){
  var notes = [];
  switch(musexpr.tag){
    case 'note':
      var newExpr = {}
      newExpr.tag = musexpr.tag;
      newExpr.pitch = convertPitch(musexpr.pitch);
      newExpr.dur = musexpr.dur;
      newExpr.start = time;
      notes.push(newExpr);
      break;
    case 'rest':
      musexpr.start = time;
      notes.push(musexpr);
      break;
    case 'seq':
      notes = notes.concat(compileT(time,musexpr.left),compileT(endTime(time,musexpr.left),musexpr.right));
      break;
    case 'par':
      notes = notes.concat(compileT(time,musexpr.left),compileT(time,musexpr.right));
      break;
    case 'repeat':
      duration = endTime(0,musexpr.section);
      for (var i = 0; i <= musexpr.count - 1; i++) {
        notes = notes.concat(compileT(time+duration*i,musexpr.section));
      };
      break;
  }
  return notes;
};

var compile = function (musexpr) {
  return compileT(0,musexpr);
};

exports.compile = compile;
