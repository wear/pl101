chai = require 'chai'
chai.should()

evalScheem = require "../src/scheem.coffee"

describe 'scheem',->
  it 'should support if condition',->
    evalScheem(['if', ['=', 1, 1], 2, 3], {}).should.equal 2
  it 'should return 3 for (if (= 1 0) 2 3)',->
    evalScheem(['if', ['=', 1, 0], 2, 3], {}).should.equal 3
  it 'should return 2 for (if (= 1 1) 2 error)',->
    evalScheem(['if', ['=', 1, 1], 2, 'error'], {}).should.equal 2
  it 'should return 3 for (if (= 1 1) error 3)',->
    evalScheem(['if', ['=', 1, 0], 'error', 3], {}).should.equal 3
  it 'should return 11 for (if (= 1 1) (if (= 2 3) 10 11) 12)',->
    evalScheem(['if',
      ['=', 1, 1],
      ['if', ['=', 2, 3], 10, 11],
      12], {}).should.equal 11
  describe 'error',->
    it 'should throw if set undefined variables',->
      (-> evalScheem(['set!','x',5],{})).should.throw "dosen't exsit"

