evalScheem = (expr,env)->
  return expr if typeof expr == 'number'
  return env[expr] if typeof expr == 'string'

  switch(expr[0])
    when '+' then evalScheem(expr[1],env) + evalScheem(expr[2],env)
    when '-' then evalScheem(expr[1],env) - evalScheem(expr[2],env)
    when '*' then evalScheem(expr[1],env) * evalScheem(expr[2],env)
    when '/' then evalScheem(expr[1],env) / evalScheem(expr[2],env)
    when 'define'
      env[expr[1]] = evalScheem(expr[2],env)
      0
    when 'set!'
      if env[expr[1]]
        env[expr[1]] = evalScheem(expr[2],env)
        0
      else
        throw new Error(expr[1] + " dosen't exsit")
    when 'begin'
      res = for i in [1..expr.length-1]
        evalScheem(expr[i],env)
      res.pop()
    when 'quote'
      expr[1]
    when '='
      if evalScheem(expr[1], env) == evalScheem(expr[2], env) then '#t' else '#f'
    when '<'
      if evalScheem(expr[1], env) < evalScheem(expr[2], env) then '#t' else '#f'
    when 'cons'
      res = []
      res = res.concat evalScheem(expr[2], env)
      res.unshift evalScheem(expr[1], env)
      res
    when 'car'
      evalScheem(expr[1], env).shift()
    when 'cdr'
      res = evalScheem(expr[1], env)
      res.shift()
      res
    when 'if'
      if evalScheem(expr[1], env) == '#t' then evalScheem(expr[2], env) else evalScheem(expr[3], env)

    else 'not supported expr'

if typeof module == "object" && typeof module.exports == "object"
  module.exports = evalScheem
else
  window.evalScheem = evalScheem
