var peg = require("pegjs");
var $ = require('jquery');
var codemirror = require('codemirror');

$(function() {
  var editor = CodeMirror.fromTextArea(document.getElementById("sheemExpr"), {
    lineNumbers: true
  });

  var sheemParserExpr = $("#pegExpr").val();
  var parser = peg.buildParser(sheemParserExpr);

  $("#parseSheemBtn").click(function(){
    $(".notes").html('');
    var expr = editor.getValue();
    try
      {
        $(".result").html(evalScheem(parser.parse(expr)));
      }
    catch(err)
      {
        $(".result").html("解析出错了："+err.message);
      }
  });

  $("#tipButton").click(function(){
    $(".hinttext").toggle();
  })
});

