chai = require 'chai'
chai.should()

peg_parser = require '../src/parser.coffee'
parser = peg_parser.mus_parser

describe 'Mus parser',->
  describe 'parse mus',->
    melody_mus1 = [
        {tag: 'note', pitch: 'c4', dur: 250,start: 0},
        {tag: 'note', pitch: 'c3', dur: 500,start: 250}
      ]
    melody_mus2 = [
        {tag: 'note', pitch: 'c4', dur: 250,start: 0},
        {tag: 'note', pitch: 'c3', dur: 500,start: 250},
        {tag: 'note', pitch: 'c3', dur: 500,start: 750},
        {tag: 'note', pitch: 'c3', dur: 250,start: 1250}
      ]
    melody_mus3 = [
        {tag: 'note', pitch: 'c4', dur: 250,start: 0},
        {tag: 'note', pitch: 'c3', dur: 500,start: 0},
        {tag: 'note', pitch: 'c3', dur: 600,start: 500},
        {tag: 'note', pitch: 'c3', dur: 250,start: 500}
      ]
    melody_mus4 = [
      {tag: 'note', pitch: 'c4', dur: 250,start: 0},
      {tag: 'note', pitch: 'c4', dur: 250,start: 250},
      {tag: 'note', pitch: 'c4', dur: 250,start: 500}
    ]

    melody_mus5 = [
        {tag: 'note', pitch: 'c4', dur: 250,start: 0},
        {tag: 'note', pitch: 'c3', dur: 500,start: 250},
        {tag: 'note', pitch: 'c4', dur: 250,start: 750},
        {tag: 'note', pitch: 'c3', dur: 500,start: 1000}
      ]
    it 'should parse one mus',->
      parser.parse("(seq,note-c4-250,note-c3-500)").should.eql melody_mus1
    it 'should parse multi mus',->
      res = parser.parse("(seq,(seq,note-c4-250,note-c3-500),(seq,note-c3-500,note-c3-250))")
      res.should.eql melody_mus2
    it 'should parse multi mus harmonies',->
      res = parser.parse("(seq,(par,note-c4-250,note-c3-500),(par,note-c3-600,note-c3-250))")
      res.should.eql melody_mus3
    it 'should parse repeat tag',->
      parser.parse("(repeat,section(note-c4-250),3)").should.eql melody_mus4
    it 'should parse repeat tag with expr',->
      parser.parse("(repeat,section((seq,note-c4-250,note-c3-500)),2)").should.eql melody_mus5
    it 'should support multi spaces',->
      parser.parse("   (   seq,note-c4-250,  note-c3-500  )").should.eql melody_mus1






