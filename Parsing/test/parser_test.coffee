chai = require 'chai'
chai.should()

peg_parser = require '../src/parser.coffee'
parser = peg_parser.basic_parser

describe 'Parsing using PEG',->
  describe 'Scheem expressions',->
    it 'should parse atom',->
      parser.parse('atom').should.eql 'atom'
    it 'should parse number',->
      parser.parse('1').should.eql 1
    it 'should parse short expressions',->
      parser.parse("(+ x 3)").should.eql ['+','x',3]
    it 'should parse multi expressions',->
      parser.parse("(+ 1 (f x 3 y))").should.eql ["+", 1, ["f", "x", 3, "y"]]
    it 'should support <',->
      parser.parse("(< 1 2)").should.eql ['<',1,2]
    it 'should support <',->
      parser.parse("(> 1 2)").should.eql ['>',1,2]
    it 'should support -',->
      parser.parse("(- 1 2)").should.eql ['-',1,2]
    it 'should support *',->
      parser.parse("(* 1 2)").should.eql ['*',1,2]
    it 'should support /',->
      parser.parse("(/ 1 2)").should.eql ['/',1,2]
    it 'should support nil',->
      parser.parse("(+ 1 nil)").should.eql ['+',1,null]
    it 'should parse (if (= 1 1) (if (= 2 3) 10 11) 12)',->
      parser.parse("(if (= 1 1) (if (= 2 3) 10 11) 12)").should.eql ['if',
      ['=', 1, 1],
      ['if', ['=', 2, 3], 10, 11],
      12]
    describe 'new feature',->
      describe 'homework part one',->
        it 'should allow any number of spaces',->
          parser.parse("(+   x      3   )").should.eql ['+','x',3]
        it 'should allow spaces around parentheses',->
          parser.parse("   (  +   x      3  )  ").should.eql ['+','x',3]
        it 'should allow newlines',->
          text = "(
          +
          x
          3)"
          parser.parse(text).should.eql ['+','x',3]
        it 'should allow tabs',->
          text = "( +
          x
          3)"
          parser.parse(text).should.eql ['+','x',3]
        it 'should parse Quotes',->
          parser.parse("'(1 2 3)").should.eql ['quote',[1,2,3]]
        it 'should parse Quotes with multi expressions',->
          parser.parse("'(1 (+ 1 3) 3)").should.eql [ 'quote', [ 1, [ '+', 1, 3 ], 3 ] ]

