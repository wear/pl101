var peg = require("pegjs");
var $ = require('jquery');
var codemirror = require('codemirror');

$(function() {
  var editor = CodeMirror.fromTextArea(document.getElementById("musExpr"), {
    lineNumbers: true
  });

  var musParserExpr = $("#pegExpr").val();
  var parser = peg.buildParser(musParserExpr);
  $("#parseMusBtn").click(function(){
    $(".notes").html('');
    var expr = editor.getValue();
    try
      {
        $(".notes").html(JSON.stringify(parser.parse(expr)));
      }
    catch(err)
      {
        $(".notes").html("解析出错了："+err.message);
      }
  });

  $("#tipButton").click(function(){
    $(".hinttext").toggle();
  })
});

