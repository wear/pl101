PEG = require("pegjs")
fs = require('fs')

basic_parser = fs.readFileSync 'src/parser.peg','utf-8'
exports.basic_parser = PEG.buildParser(basic_parser)

mus_parser = fs.readFileSync 'src/mus_parser.peg','utf-8'
exports.mus_parser = PEG.buildParser(mus_parser)
