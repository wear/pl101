reversed_keywords =
  '#f': '#f'
  '#t': '#t'
  '+': (x,y)-> x+y
  '-': (x,y)-> x-y
  '*': (x,y)-> x*y
  '/': (x,y)-> x/y
  '=': (x,y)-> if x == y then '#t' else '#f'
  '>': (x,y)-> if x > y then '#t' else '#f'
  '<': (x,y)-> if x < y then '#t' else '#f'
  'list': (x)-> [x]
  'append': (lista,listb) ->
    if lista != null and listb != null
      return lista.concat listb
    return listb if listb != null
    return lista if lista != null
  'cons': (x,y)->
    if y instanceof Array
      y.unshift x
      return y
    else
      throw new Error('last argument must be list')
  'car': (list)->
    if list instanceof Array and list.length >= 1 then list[0] else null
  'cdr': (list)->
    if list instanceof Array and list.length > 1 then list.slice(1) else null
  'list?': (list)->
    if list instanceof Array then '#t' else '#f'
  # 'alert': (x)-> if runByNode then console.log(x) else alert(x)

lookup = (env,v)->

  if env.bindings[v] != undefined
    return env.bindings[v]
  else
    if !env.outer
      return reversed_keywords[v] if reversed_keywords[v]
      throw new Error(v + " undefined")
    lookup env.outer,v

module.exports = lookup
