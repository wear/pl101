lookup = require "./lookup"
update = require "./update"

runByNode = -> typeof module == "object" && typeof module.exports == "object"

evalScheem = (expr,env)->
  env.bindings = {} if !env.bindings
  return expr if expr == null
  return expr if typeof expr == 'number'
  return lookup(env,expr) if typeof expr == 'string'
  switch(expr[0])
    when 'let'
      bnds = {}
      for item in expr[1]
        bnds[item[0]] = evalScheem(item[1],env)
      newEnv =
        bindings:bnds,
        outer:env
      return evalScheem(expr[2],newEnv)
    when 'define'
      env.bindings[expr[1]] = evalScheem(expr[2],env)
      0
    when 'set!'
      update(env,expr[1],evalScheem(expr[2],env))
      0
    when 'begin'
      res = for i in [1..expr.length-1]
        evalScheem(expr[i],env)
      res.pop()
    when 'quote'
      expr[1]
    when 'if'
      if evalScheem(expr[1], env) == '#t' then evalScheem(expr[2], env) else evalScheem(expr[3], env)
    when 'lambda'
      func = (args)->
        if expr[1].length >= 1
          bnds = {}
          for item, i in expr[1]
            bnds[item] = arguments[i]
          newEnv =
            bindings:bnds,
            outer:env
          evalScheem(expr[2],newEnv)
        else
          evalScheem(expr[2],env)
      func

    else
      func = evalScheem(expr[0],env)
      args = if expr.length > 1
        expr.slice(1).map (expr)-> evalScheem(expr,env)
      else
        []
      func.apply null,args

if runByNode()
  module.exports = evalScheem
else
  window.evalScheem = evalScheem



