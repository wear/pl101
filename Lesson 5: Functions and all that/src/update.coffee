update = (env,v,val)->
  throw new Error(v + " undefined") if !env.bindings
  if env.bindings[v]
    env.bindings[v] = val
  else
    update(env.outer,v,val)

module.exports = update
