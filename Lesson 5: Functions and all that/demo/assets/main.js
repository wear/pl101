var peg = require("pegjs");
var $ = require('jquery');
var codemirror = require('codemirror');
var evalScheem = require('./scheem')

$(function() {
  var editor = CodeMirror.fromTextArea(document.getElementById("sheemExpr"), {
    lineNumbers: true
  });

  var sheemParserExpr = $("#pegExpr").val();
  var parser = peg.buildParser(sheemParserExpr);

  $("#parseSheemBtn").click(function(){
    $(".notes").html('');
    var expr = editor.getValue();
    try
      {
        $(".parseResult").html(JSON.stringify(parser.parse(expr)));
        $(".result").html(JSON.stringify(evalScheem(parser.parse(expr),{})));
      }
    catch(err)
      {
        $(".result").html("解析出错了："+err.message);
      }
  });

  $(".example").click(function(e){
    switch($(this).attr('id')){
      case 'one':
        editor.setValue("(begin (define factorial (lambda (n) (if (= n 1) 1 (* n (factorial(- n 1)))))) (factorial 4))");
        break;
      case 'two':
        editor.setValue("(begin (define fibonacci (lambda (n) (if (< n 2) n (+ (fibonacci (- n 1)) (fibonacci (- n 2)))))) (fibonacci 10))");
        break;
      case 'three':
        editor.setValue("(begin (define reverseList (lambda (n) (if (= n nil) nil (append (reverseList (cdr n)) (list (car n)))))) (reverseList '(1 2 3)))");
        break;
      case 'four':
        editor.setValue("(begin (define included (lambda (list el) (if (= list nil) #f (if (= el (car list)) #t (included (cdr list) el))))) (included '(1 2 3) 5))")
        break;
      case 'five':
        editor.setValue("(begin (define map (lambda (tlist fn) (if (= tlist nil) nil (append (list (fn (car tlist))) (map (cdr tlist) fn))))) (map '(1 2 3) (lambda (n) (+ n 1))))")
        break;
      case 'six':
        editor.setValue("(begin (define flatten (lambda (tlist) (if (= tlist nil) nil (append (if (list? (car tlist)) (flatten (car tlist)) (list (car tlist))) (flatten (cdr tlist)))))) (flatten '(1 (2 3 (4 5)))))");
        break;
    }
  })

  $("#tipButton").click(function(){
    $(".hinttext").toggle();
  })
});

