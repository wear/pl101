chai = require 'chai'
chai.should()

lookup = require "../src/lookup.coffee"

env1 = { bindings: {'x': 19}, outer: { } };
env2 = { bindings: {'y': 16}, outer: env1 };
env3 = { bindings: {'x': 2}, outer: env2 };
env4 = { bindings: { 'x': 0 }, outer: { bindings: {} } }

describe 'lookup',->
  it 'should support Single binding',->
    lookup(env1,'x').should.equal 19
  it 'should support Double binding inner',->
    lookup(env2,'y').should.equal 16
  it 'should support Double binding outer',->
    lookup(env2,'x').should.equal 19
  it 'should support Triple binding inner',->
    lookup(env3,'x').should.equal 2
  it 'should support 0',->
    lookup(env4,'x').should.equal 0
  # it 'should raise if no bindings',->
  #   (-> lookup({},'x')).should.throw "x undefined"
