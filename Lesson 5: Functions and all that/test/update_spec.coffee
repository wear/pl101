chai = require 'chai'
chai.should()

update = require "../src/update.coffee"

env1 =
  bindings:
    x: 19

  outer: {}

env1u =
  bindings:
    x: 20

  outer: {}

env2 =
  bindings:
    y: 16

  outer:
    bindings:
      x: 19

    outer: {}

env2u =
  bindings:
    y: 10

  outer:
    bindings:
      x: 19

    outer: {}

env2v =
  bindings:
    y: 10

  outer:
    bindings:
      x: 20

    outer: {}

env3 =
  bindings:
    x: 2

  outer:
    bindings:
      y: 16

    outer:
      bindings:
        x: 19

      outer: {}

env3u =
  bindings:
    x: 9

  outer:
    bindings:
      y: 16

    outer:
      bindings:
        x: 19

      outer: {}

describe 'update',->
  it "Single binding",->
    update env1, "x", 20
    env1.should.eql env1u
  it "Double binding inner",->
    update env2, "y", 10
    env2.should.eql env2u
  it "Double binding outer",->
    update env2, "x", 20
    env2.should.eql env2v
  it "Triple binding inner",->
    update env3, "x", 9
    env3.should.eql env3u
  it 'should raise if no bindings',->
    (-> update({},'x',3)).should.throw "x undefined"
