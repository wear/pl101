chai = require 'chai'
chai.should()
expect = chai.expect

evalScheem = require "../src/scheem.coffee"

describe 'reserved keywords',->
  it 'should suport addition',->
    evalScheem(['+',1,2],{}).should.equal 3
  it 'should suport addition',->
    evalScheem(['+',['+',2,3],2],{}).should.equal 7
  it 'should supprt -',->
    evalScheem(['-',2,1],{}).should.equal 1
  it 'should supprt *',->
    evalScheem(['*',2,3],{}).should.equal 6
  it 'should supprt /',->
    evalScheem(['/',6,3],{}).should.equal 2
  it 'should supprt =',->
    evalScheem(['=',6,3],{}).should.equal '#f'
  it 'should supprt =',->
    evalScheem(['=',null,3],{}).should.equal '#f'
  it 'should supprt >',->
    evalScheem(['>',6,3],{}).should.equal '#t'
  it 'should supprt <',->
    evalScheem(['<',6,3],{}).should.equal '#f'
  it 'should supprt cons',->
    evalScheem(['cons',1,['quote', [2, 3]]],{}).should.eql [1, 2, 3]
  it 'should supprt cons',->
    evalScheem(['cons',['quote',[1]],['quote', [2, 3]]],{}).should.eql [[1], 2, 3]
  it 'should supprt cons',->
    evalScheem(['cons',['quote', [1, 2]],['quote', [3, 4]]],{}).should.eql [[1, 2], 3,4]
  it 'should supprt car',->
    evalScheem(['car',['quote', [1, 2]]],{}).should.equal 1
  it 'should supprt cdr',->
    evalScheem(['cdr',['quote', [1, 2, 3]]],{}).should.eql [2, 3]
  it 'should supprt cdr',->
    expect(evalScheem(['cdr',['quote', [1]]],{})).to.be.a('null')
  it 'should support list?',->
    evalScheem(['list?',2],{}).should.equal '#f'
  it 'should support list?',->
    evalScheem(['list?',['quote',[1,2,3]]],{}).should.equal '#t'
  describe 'append',->
    it 'should supprt append',->
      evalScheem(['append',['quote',[1]],null],{}).should.eql [1]
    it 'should supprt append',->
      evalScheem(['append',null,['quote',[1]]],{}).should.eql [1]
    it 'should supprt append',->
      evalScheem(['append',['quote',[2,3]],['quote',[1]]],{}).should.eql [2,3,1]
  describe 'reserved',->
    env =
      bindings:
        list: [1,2,3]
    it 'should read custom variable first',->
      evalScheem('list',env).should.eql [1,2,3]

describe 'let',->
  env1 =
    bindings:
      x: 19

    outer: {}

  env2 =
    bindings:
      y: 16

    outer: env1

  env3 =
    bindings:
      x: 2

    outer: env2

  env3_copy =
    bindings:
      x: 2

    outer: env2
  # (let ((var1 val1)
  #       (var2 val2)
  #       ...
  #      )
  #     body)
  it 'Variable reference in environment',->
    evalScheem('x', env3).should.equal 2
  # it 'Variable reference in environment',->
  #   evalScheem(['+', 'x', 'y'], env3).should.equal 18
  it 'let with computed value',->
    evalScheem(['let', [['x', ['+', 2, 2]]], 'x'], env3).should.equal 4
  it 'environment did not change',->
    env3.should.eql env3_copy
  it 'let with environment, inner reference',->
    evalScheem(['let', [['z', 7]], 'z'], env3).should.equal 7
  it 'let with environment, outer reference',->
    evalScheem(['let', [['x', 7]], 'y'], env3).should.equal 16
  it 'let with environment, outer reference',->
    evalScheem(['let', [['x', 7]], 'y'], env3).should.equal 16

describe 'function as value',->
  always3 = (x) ->
    3

  identity = (x) ->
    x

  plusone = (x) ->
    x + 1

  env =
    bindings:
      always3: always3
      identity: identity
      plusone: plusone

    outer: {}
  it "(always3 5)",->
    evalScheem(["always3", 5], env).should.equal 3
  it "(identity 5)",->
    evalScheem(["identity", 5], env).should.equal 5
  it "(plusone 5)",->
    evalScheem(["plusone", 5], env).should.equal 6
  it "(plusone (always3 5))",->
    evalScheem(["plusone", ["always3", 5]], env).should.equal 4
  it "(plusone (+ (plusone 2) (plusone 3)))",->
    evalScheem(["plusone", ["+", ["plusone", 2], ["plusone", 3]]], env).should.equal 8

# (lambda (x) (+ x 3))
describe 'lambda',->
  it "((lambda (x) x) 5)",->
    evalScheem([["lambda", ["x"], "x"], 5], {}).should.equal 5
  it "((lambda (x) (+ x 1)) 5)",->
    evalScheem([["lambda", ["x"], ["+", "x", 1]], 5], {}).should.equal 6
  it "((lambda (x y) (+ x y)) 1 2)",->
    evalScheem([["lambda", ["x",'y'], ["+", "x", 'y']], 1, 2], {}).should.equal 3
  it "((lambda (x y z) (+ x (+ y z))) 1 2 3)",->
    evalScheem([["lambda", ["x",'y','z'], ['+','x',['+','y','z']]], 1, 2, 3], {}).should.equal 6
  it "(((lambda (x) (lambda (x) (+ x x))) 5) 3)",->
    evalScheem([[["lambda", ["x"], ["lambda", "x", ["+", "x", "x"]]], 5], 3], {}).should.equal 6
  it 'should suport no args',->
    evalScheem([['lambda',[],5],7],{}).should.equal 5


describe 'interesting',->
  it 'Defining a function',->
    env =
      bindings:{}
    evalScheem(['define','plusone',['+',2,2]],env)
    evalScheem('plusone',env).should.equal 4
  it 'Defining simple function',->
    evalScheem(['begin',['define','plusone',['lambda',['x'],['+','x',1]]],['plusone',5]],{}).should.equal 6
  it 'Simple function calls',->
    env =
      bindings:
        plusone: (x)-> x + 1
    evalScheem(['plusone',5],env).should.equal 6
  it 'Calling an anonymous function',->
    evalScheem([["lambda", ["x"], "x"], 5], {}).should.equal 5
  it 'Passing a function as a value to another function',->
    env =
      bindings:
        plusone: (x)-> x + 1
    evalScheem([['lambda',['x'],['plusone','x']],8],env).should.equal 9
  it 'Inner function uses values from enclosing function',->
    evalScheem(['begin',
      ['define','makeAccount',['lambda',['blance'],['lambda',['amount'],['+','amount','blance']]]],
      ['define','account',['makeAccount',100]],
      ['account',20]],{}).should.equal 120
  it 'Argument to a function shadows a global variable',->
    env =
      bindings:
        x: 5
    evalScheem(['begin',
      ['define','change_global',['lambda',['v'],['set!','x','v']]],
      ['change_global',10],'x'],env).should.equal 10

  it 'A function in a define that calls itself recursively',->
    evalScheem(['begin',
      ['define','good_enough?',['lambda',['x'],['if',['>','x',10],10,['good_enough?',['+','x',1]]]]],
      ['good_enough?',1]],{}).should.equal 10

describe 'if',->
  it 'should supprt >',->
    evalScheem(['>',10,5],{}).should.equal '#t'
  it 'should support if',->
    evalScheem(['if',['>',10,5],10,2],{}).should.equal 10

describe 'set!',->
  env =
    bindings:
      x: 5
  it 'should change variable',->
    evalScheem(['set!','x',10],env)

describe 'Function application',->
  add_three = (x, y, z) ->
    x + y + z
  say_hellp = ->
    'hell world'

  env =
    bindings:
      add_three: add_three
      say_hellp: say_hellp

  it 'should call three args',->
    evalScheem(['add_three',1,2,3],env).should.equal 6
  it 'should call for args',->
    evalScheem(['add_three',1,2,['+' ,1, 8]],env).should.equal 12
  it 'should call no args',->
    evalScheem(['say_hellp'],env).should.equal 'hell world'

describe.only "demo",->
  it 'Compute factorial',->
    evalScheem(['begin',['define','factorial',['lambda',['n'],['if',['=','n',1],1,['*','n',['factorial',['-','n',1]]]]]],['factorial',4]],{}).should.equal 24
  it 'Compute Fibonacci numbers',->
    evalScheem(['begin',['define','fibonacci',
      ['lambda',['n'],['if',['<','n',2],'n',['+',['fibonacci',['-','n',1]],['fibonacci',['-','n',2]]]]]],['fibonacci',10]],{}).should.equal 55
  it 'Reverse a list',->
    evalScheem(['begin',['define','reverse',['lambda',['n'],['if',['=','n',null],null,['append',['reverse',['cdr','n']],['list',['car','n']]]]]],['reverse',['quote',[1,2,3]]]],{}).should.eql [3,2,1]
  it 'Find if an element is in a list.',->
    evalScheem(['begin',['define','included',['lambda',['list','el'],['if',['=','list',null],'#f',['if',['=','el',['car','list']],'#t',['included',['cdr','list'],'el']]]]],['included',['quote',[1,2,3]],5]],{}).should.equal '#f'
  it 'Apply a function to every element of a list.',->
    evalScheem(['begin',['define','map',['lambda',['tlist','fn'],['if',['=','tlist',null],null,['append',['list',['fn',['car','tlist']]],['map',['cdr','tlist'],'fn']]]]],['map',['quote',[1,2,3]],['lambda',['n'],['+','n',1]]]],{}).should.eql [2,3,4]
  it 'Flatten a nested list of lists into a single list.',->
    evalScheem(['begin',['define','flatten',['lambda',['tlist'],['if',['=','tlist',null],null,['append',['if',['list?',['car','tlist']],['flatten',['car','tlist']],['list',['car','tlist']]],['flatten',['cdr','tlist']]]]]],['flatten',['quote',[1,[2,3,[4,[5]]],6]]]],{}).should.eql [1,2,3,4,5,6]
